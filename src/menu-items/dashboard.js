// assets
import { IconDashboard } from '@tabler/icons';
import { IconFileZip } from '@tabler/icons';
import { IconFileInfo } from '@tabler/icons';

// constant
const icons = { IconDashboard, IconFileInfo, IconFileZip };

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const dashboard = {
    id: 'dashboard',
    title: 'Dashboard',
    type: 'group',
    children: [
        {
            id: 'default',
            title: 'Dashboard',
            type: 'item',
            url: '/dashboard/default',
            icon: icons.IconDashboard,
            breadcrumbs: false
        },
        {
            id: 'serviceImage',
            title: 'Service Image Form',
            type: 'item',
            url: '/service-image-form',
            icon: icons.IconFileZip,
            breadcrumbs: false
        },
        {
            id: 'serviceImage',
            title: 'Service Feed Form',
            type: 'item',
            url: '/service-image-form',
            icon: icons.IconFileInfo,
            breadcrumbs: false
        },
        {
            id: 'serviceImage',
            title: 'Contact',
            type: 'item',
            url: '/service-image-form',
            icon: icons.IconDashboard,
            breadcrumbs: false
        },
        {
            id: 'serviceImage',
            title: 'Account',
            type: 'item',
            url: '/service-image-form',
            icon: icons.IconDashboard,
            breadcrumbs: false
        }
    ]
};

export default dashboard;
