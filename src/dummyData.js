export const dummyData = [
  {
    id: 1,
    services: "Accounting Services",
  },
  {
    id: 2,
    services: "Income Tax Filling",
  },
  {
    id: 1,
    services: "Accounting Services",
  },
  {
    id: 2,
    services: "Income Tax Filling",
  },
  {
    id: 3,
    services: "Registration",
    registration: [
      {
        id: 1,
        register: "GST Registration and Returns",
      },
      {
        id: 2,
        register: "Gomasta",
      },
      {
        id: 3,
        register: "Udhyam Registration",
      },
      {
        id: 4,
        register: "Professional Tax Registration",
      },
      {
        id: 5,
        register: "Company Registration",
      },
      {
        id: 6,
        register: "PAN Card",
      },
    ],
  },
  {
    id: 4,
    services: "Audit",
    Auditors: [
      {
        id: 1,
        register: "Internal Audit/Management Audit/SOP",
      },
      {
        id: 2,
        register: "Stock Audit",
      },
      {
        id: 3,
        register: "Tax Audits",
      },
    ],
  },
  {
    id: 5,
    services: "CA Certificate",
  },
  {
    id: 6,
    services: "Business Management",
  },
  {
    id: 7,
    services: "Digital Signature",
  },
  {
    id: 8,
    services: "Partnership Deed",
  },
  {
    id: 9,
    services: "TDS",
  },
  {
    id: 10,
    services: "Project Report / CMA",
  },
  {
    id: 11,
    services: "Capital Gain Calculation",
  },
  {
    id: 12,
    services: "Loan Application/ Mudra Laon",
  },
  {
    id: 13,
    services: "Appeals - Direct and Indirect",
  },
  {
    id: 14,
    services: "Consultancy",
  },
];
